require('dotenv').config()
var express = require('express');
const cors = require('cors');
var app = express();
var morgan = require('morgan')


app.use(morgan('combined'))

//encriptation
var fs = require('fs');
var bodyParser = require('body-parser');

//middleware
var corsOptions = { origin: true, optionsSuccessStatus: 200 };
app.use(cors(corsOptions));
app.use(bodyParser.json({ limit: '20mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }))


app.get('/', (req, res) => {
    res.send('Hola mundo!  :3')
})


app.listen(process.env.PORT, () => console.log(`Esperando peticiones ${process.env.PORT}`));